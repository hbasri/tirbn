-- MySQL dump 10.13  Distrib 5.7.20, for osx10.13 (x86_64)
--
-- Host: 127.0.0.1    Database: tirbn
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Politik',NULL,NULL,NULL),(2,'Hiburan',NULL,NULL,NULL),(3,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `content` text,
  `status` enum('draft','publish','deleted') DEFAULT 'draft',
  `created_by` int(10) DEFAULT NULL,
  `edited_by` int(10) DEFAULT NULL,
  `deleted_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `publish_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'Setya Novanto Kecelakaan',2,'<p>berdasarkan fakta yang beredar di masyarakat, setya novanto terlibat kecelakaan dengan kendaraan bermobil dengan nomor polisi 1234 c.</p>\r\n\r\n<p><img alt=\"\" src=\"http://tirbn.dev/laravel-filemanager/photos/1/CAM00029.jpg\" style=\"height:150px; width:267px\" /></p>\r\n\r\n<p>mamah cantik banget sihh ???</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"http://tirbn.local/laravel-filemanager/photos/1/CAM00029.jpg\" style=\"height:100px; width:178px\" /></p>','draft',1,1,NULL,'2017-11-17 13:23:19','2017-12-08 09:22:44','2017-12-08 11:04:41',NULL),(4,'Reoni 212',1,'<p>Jakarta, CNN Indonesia -- Kehadiran Gubernur DKI Jakarta Anies Baswedan di acara Reuni Akbar Alumni 212 dinilai sebagai beban utang politik yang harus dia tanggung selama memimpin ibu kota.<br />\r\n<br />\r\nPengamat politik Universitas Indonesia Reni Suwarso menilai kehadiran Anies pada reuni 212 sebagai bentuk ungkapan rasa terima kasih. Terlebih, kata Reni, Anies selama kampanye mengambil suara dari pengaruh kelompok militan yang berhasil membalik pola kampanye, dari yang tadinya adu program menjadi adu isu SARA.&nbsp;<br />\r\n<br />\r\n&quot;Ketika mereka mengadakan acara seperti itu, mau tidak mau, Anies bayar utang politik,&quot; kata Direktur Pusat Studi Pemilu dan Partai Politik FISIP-UI (Center for Election and Political Party FISIP-UI) tersebut kepada CNNIndonesia.com, Minggu (3/12).<br />\r\n&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://newrevive.detik.com/delivery/lg.php?bannerid=0&amp;campaignid=0&amp;zoneid=1548&amp;loc=https%3A%2F%2Fwww.cnnindonesia.com%2Fnasional%2F20171204090414-32-259922%2Freuni-212-dan-beban-utang-politik-anies-baswedan%2F&amp;referer=https%3A%2F%2Fwww.google.co.id%2F&amp;cb=9c92b2ad25\" style=\"height:0px; width:0px\" /></p>\r\n\r\n<p><br />\r\nReni berpendapat, masalah baru akan merundung Anies apabila ia absen di acara yang menggaungkan semangat Negara Kesatuan Republik Indonesia (NKRI) bersyariah itu.<br />\r\n<br />\r\n&quot;Kalau mereka (Anies-Sandi) tidak ikutan, tidak ikut hadir, tidak ikut berpartisipasi, nanti dukungan terhadap mereka back off, pada mundur,&quot; kata Reni.<br />\r\n&nbsp;</p>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>Lihat juga:</p>\r\n			&nbsp;<a href=\"https://www.cnnindonesia.com/nasional/20171204074725-32-259914/sampai-jumpa-212-selamat-datang-tahun-politik/\">Sampai Jumpa 212, Selamat Datang Tahun Politik</a></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><br />\r\nKeseriusan Anies mengusung kampanye persatuan bangsa kini diragukan. Sebab dalam pidatonya di Reuni 212, Anies mengkotak-kotakan antara massa 212 dengan &#39;massa pesimis&#39;.<br />\r\n<br />\r\n&quot;Itu namanya menjilat ludah sendiri karena ketika mau menang, (Anies) kan bilang mau merangkul seluruh kelompok. Semua warga negara di Jakarta yang beda kepentingan, ras, suku, agama, dia adalah gubenurnya. Tetapi, sekarang setelah benar-benar dilantik, kok jadi balik arah?&quot; kata Reni.<br />\r\n<br />\r\nMeski demikian, Reni menganggap mungkin saja Anies tersandera secara politik, sehingga mau tidak mau, suka tidak suka, ia harus menghadiri dan memafaslitasi massa aksi semacam itu.<br />\r\n<br />\r\n&quot;Tersandera secara politik, mungkin saja. Kalau tidak suudzon, dalam hati kecilnya itu, mungkin dia terbuka dan pluralis, tetapi karena dia menang (dengan cara) seperti itu, dia jadi tersandera oleh sistem,&quot; kata wanita yang meraih gelar PhD bidang Ilmu Politik dari Victoria University, Australia itu.</p>\r\n\r\n<table align=\"center\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><img alt=\"Reuni 212 dan Beban Utang Politik Anies Baswedan\" src=\"https://images.detik.com/community/media/visual/2017/12/02/c6ac4585-f52b-452f-9160-e8d324ed6774_169.jpg?w=620\" />Massa alumni melakukan Shalat Subuh berjamaah Alumni 212 di Monumen Nasional (2/12). (CNN Indonesia/Adhi Wicaksono)</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>Lihat juga:</p>\r\n			&nbsp;<a href=\"https://www.cnnindonesia.com/nasional/20171203152330-32-259825/survei-anies-ahy-dan-ahok-dilirik-jadi-cawapres-2019/\">Survei: Anies, AHY, dan Ahok Dilirik Jadi Cawapres 2019</a></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><br />\r\nAdapun maksud &#39;tersandera&#39;, Reni berujar, Anies mungkin sudah terjebak dalam situasi politik semacam ini. &quot;Kecuali ada kekuatan besar atau dia punya kemauan besar untuk keluar dari jebakan di sini.&quot;<br />\r\n<br />\r\nSenada, pengamat politik Universitas Padjajaran Idil Akbar juga menilai bahwa kehadiran Anies di reuni 212 bisa dibilang sebagai tanda terima kasih atas peran massa pada aksi tahun lalu. Menurut Idil, ada dua sisi yang bisa dilihat dari kehadiran Anies pada reuni 212.&nbsp;<br />\r\n<br />\r\nPertama, Anies sebagai tuan rumah Jakarta, kota diselenggarakannya perhelatan akbar yang diklaim mempersatukan umat muslim dari berbagai daerah itu.<br />\r\n<br />\r\n&quot;Oleh karena itu, mereka (Anies-Sandi) perlu memberi penghormatan kepada tamu-tamu yang datang ke rumahnya dengan cara hadir di lokasi,&quot; kata Idil.<br />\r\n<br />\r\nKedua, berkaitan dengan politik.&nbsp;<br />\r\n<br />\r\n&quot;Karena kita tahu bahwa salah satu hal yang mempengaruhi kemenangan Anies adalah mereka, umat Islam tadi yang kemudian concern terhadap Jakarta yang tidak ingin dipimpin oleh Ahok dan juga dipicu oleh pernyataan-pernyataan Ahok yang memicu umat Islam untuk kemudian memilih Anies,&quot; kata Idil.<br />\r\n&nbsp;</p>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>Lihat juga:</p>\r\n			&nbsp;<a href=\"https://www.cnnindonesia.com/nasional/20171202162225-21-259685/wajah-wajah-cemas-rakyat-di-reuni-akbar-212/\">Wajah-wajah Cemas Rakyat di Reuni Akbar 212</a></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><br />\r\nDi sela rangkaian acara Reuni 212, Anies berpidato selama kurang lebih 18 menit usai jemaah menunaikan ibadah salat Subuh.<br />\r\n<br />\r\nDalam pidatonya, Anies menyebut massa reuni 212 telah mengecewakan kaum pesimis. Menurutnya, kaum pesimis adalah mereka yang berpikir negatif, bahwa berkumpulnya massa akan menimbulkan kericuhan, kekerasan, dan ketidakdamaian.&nbsp;<br />\r\n<br />\r\nAnies lantas mengakhiri pidatonya dengan memberikan salam kepada semua warga di luar Jakarta.&nbsp;<br />\r\n<br />\r\n&quot;Salam dari warga ibu kota kepada warga di tempat saudara-saudara berasal, sampaikan kepada semua, (bahwa) pergerakan perubahan sedang berjalan di kota ini,&quot; kata Anies yang kembali disambut oleh takbir dari hadirin.<br />\r\n<br />\r\nAnies pun memastikan bahwa kota yang ia pimpin sejak delapan minggu lalu itu, ramah untuk semua kalangan.<br />\r\n<br />\r\n&quot;Kembalilah nanti ke kampung dengan membawa pesan, kita akan terus mengawal dan memastikan bahwa Jakarta menjadi kota milik semua, bukan sebagian,&quot; kata Anies.</p>','draft',1,NULL,NULL,'2017-12-08 09:57:07',NULL,NULL,NULL);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_tags`
--

DROP TABLE IF EXISTS `news_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_tags`
--

LOCK TABLES `news_tags` WRITE;
/*!40000 ALTER TABLE `news_tags` DISABLE KEYS */;
INSERT INTO `news_tags` VALUES (18,1,2),(19,1,3),(21,4,14),(22,4,15),(23,4,3);
/*!40000 ALTER TABLE `news_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publications`
--

DROP TABLE IF EXISTS `publications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `file` varchar(255) DEFAULT NULL,
  `linkfile` varchar(255) DEFAULT NULL,
  `extention` varchar(5) DEFAULT NULL,
  `status` enum('show','not show') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publications`
--

LOCK TABLES `publications` WRITE;
/*!40000 ALTER TABLE `publications` DISABLE KEYS */;
INSERT INTO `publications` VALUES (1,'Laporan Penelitian Sistem Pintar','-','vps tata.docx','4038b4772bae0c45266a580efc71d9a9.docx','docx','show','2017-12-08 12:22:49','2017-12-08 12:22:49',NULL);
/*!40000 ALTER TABLE `publications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'setyanovanto'),(2,'setnov'),(3,'politik'),(12,'papa setnov'),(13,'indonesia'),(14,'reoni212'),(15,'jakarta');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` enum('staff','administrator') COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Hasan','hasan','$2y$10$AI01Q5XN13uK7sytaTykaOQrLeDUwLOwML1fzIvyDkXhMJ3UHh6hu','administrator','4X1Ma17i5JTWd6OlUjBpKcQdBdRqiLogZb8NEMlRnmsR4oVo5YUQCYEGtrDu','2017-10-29 00:29:06','2017-12-26 11:27:29',NULL,'2017-12-26 11:27:29'),(2,'Basri','basri','$2y$10$eQe8OEWS5lwcTfVQqdfSGuXlIJkGncFh8txxQET9rBq2RYudoJ0nK','staff',NULL,'2017-10-29 00:29:06','2017-10-29 00:29:06',NULL,NULL),(3,'Fahmi','fahmi','$2y$10$.4UMZ.SDS7FOVQIYqCq0e.cWjZcrILxjz480pRBjpTXxk/5KDwVii','staff',NULL,'2017-10-31 15:08:41','2017-10-31 15:21:04','2017-10-31 15:21:04',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-26 19:32:50
