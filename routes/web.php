<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get("/login",[ 'as' => 'login', 'uses' => "Auth\LoginController@showLoginForm"]);
Route::get('/home', 'HomeController@index')->name('home');
Route::post("/testEditor","HomeController@save");

Route::group(['middleware'=>'auth'],function(){

    Route::prefix('master')->group(function () {
        //user
        Route::get("users","UserController@index");
        Route::get("user/show/{id}","UserController@show");
        Route::get("user/create","UserController@create");
        Route::post("user","UserController@store");
        Route::get("user/edit/{id}","UserController@edit");
        Route::patch("user/{id}","UserController@update");
        Route::delete("user/{id}","UserController@delete");

        //news
        Route::get("news","NewsController@index");
        Route::get("news/show/{id}","NewsController@show");
        Route::get("news/create","NewsController@create");
        Route::post("news","NewsController@store");
        Route::get("news/edit/{id}","NewsController@edit");
        Route::patch("news/{id}","NewsController@update");
        Route::delete("news/{id}","NewsController@delete");

        //gallery
        Route::post("gallery/uploads", "GalleryController@uploads");
        Route::patch("gallery/uploads/edit/{id}", "GalleryController@uploadById");
        Route::delete("gallery/delete-photo/{id}","GalleryController@deletePhoto");
        Route::get("gallery","GalleryController@index");
        Route::get("gallery/create", "GalleryController@create");
        Route::post("gallery","GalleryController@store");
        Route::get("gallery/edit/{id}","GalleryController@edit");
        Route::patch("gallery/{id}","GalleryController@update");
        Route::delete("gallery/{id}","GalleryController@delete");
        Route::post("gallery/check-albumname", "GalleryController@validateAlbumnameExist");
        Route::post("gallery/check-albumname/{id}", "GalleryController@validateAlbumnameExistById");

        //publikasi
        Route::get("publications","PublicationController@index");
        Route::get("publication/create","PublicationController@create");
        Route::post("publication","PublicationController@save");
        Route::get("publication/edit/{id}","PublicationController@edit");
        Route::patch("publication/{id}","PublicationController@update");
        Route::delete("publication/{id}","PublicationController@delete");
        Route::delete("publication/file/{id}","PublicationController@deleteFile");

        //tags
        Route::get("get/tags","NewsController@getTags");

    });
});
