<html>
<head>
    <title></title>
</head>
<body>
    <form enctype="multipart/form-data" method="post" action="{{ url('testEditor') }}">
        {{ csrf_field() }}
        <textarea rows="10" cols="50" name="texteditor"></textarea>
        <br>
        <button type="submit" name="submit">Submit</button>
    </form>

</body>
<script   src="https://code.jquery.com/jquery-3.2.1.min.js"   integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="   crossorigin="anonymous"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script>
    var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
</script>
<script>
    $('textarea').ckeditor(options);
    // $('.textarea').ckeditor(); // if class is prefered.
</script>

</html>