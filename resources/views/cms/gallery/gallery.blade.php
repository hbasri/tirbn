@if(count($galleries) > 0)
    @foreach($galleries as $gallery)
        <tr>
            <td>{{ (!empty($gallery->albumname) ? $gallery->albumname : "-") }}</td>
            <td>{{ !empty($gallery->desc) ? $gallery->desc : "-" }}</td>
            <td>
                @if($gallery->status == "show")
                    <span class="label label-success">Ditampilkan</span>
                @elseif($gallery->status == "not show")
                    <span class="label label-danger">Tidak Ditampilkan</span>
                @endif
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary">Aksi</button>
                    <button type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url("master/gallery/edit/".$gallery->id) }}">Ubah</a>
                        </li>
                        <li><a href="{{ url("master/gallery/".$gallery->id) }}"
                               data-method="delete" data-confirm="Hapus Data Ini?"
                               data-token="{{ csrf_token() }}">Hapus</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="6">Data Masih Kosong</td>
    </tr>
@endif