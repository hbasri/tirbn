@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/plugins/iCheck/all.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/select2/css/select2.css") }}">
    <style type="text/css" media="screen">
        .gallery{
            width: 200px;
            padding: 5px;
            margin: 7px;
            min-height: 200px;
            border: 1px solid #ddd;
            overflow: hidden;
        }

        .thumbnail{
            width: 188px;
            line-height: 20px;
            margin-bottom: 5px;
            overflow: hidden;
            word-break: normal;
        }
    </style>
@endsection
@section("content")
    <section class="content-header">
        <h1>
            Gallery
            <small>Data Gallery</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Gallery</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Tambah Gallery</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ url('master/gallery') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('albumname') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Nama Album</label>
                                <input type="text" name="albumname" class="form-control albumname" placeholder="Nama">
                                @if($errors->has("albumname"))
                                    <span class="help-block">{{ $errors->first("albumname") }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Deskripsi</label>
                                <textarea class="form-control" name="desc">{{ old("desc") }}</textarea>

                            </div>

                            <div class="form-group has-feedback {{ $errors->has('file_ids') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Foto</label>
                                <input type="file" class="form-control" id="fileupload" name="photos[]" data-url="{{url('master/gallery/uploads')}}" multiple disabled>
                                @if($errors->has("file_ids"))
                                    <span class="help-block">{{ $errors->first("file_ids") }}</span>
                                @endif
                                <br />
                                <div id="files_list" class="gallery"></div>
                                <p id="loading"></p>
                                <input type="hidden" name="file_ids" id="file_ids" value="" />
                                <input type="hidden" name="path" id="path" value="" />
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('status') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Status Tampil</label>
                                <br/>
                                <label>
                                    <input type="radio" name="status" value="show" class="flat-red" checked> Ditampilkan
                                </label>
                                <label>
                                    <input type="radio" name="status" value="not show" class="flat-red" {{ old("status") == "not show" ? "checked" : "" }}> Tidak Ditampilkan
                                </label>

                            </div>
                            <input type="hidden" name="status_upload" value="done">
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ url()->previous() }}">
                                <button type="button" class="btn btn-default">Batal</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("js_plugins")
    <script src="{{ asset("assets/plugins/iCheck/icheck.min.js") }}"></script>
    <script src="{{ asset("assets/select2/js/select2.js") }}"></script>
    <script src="{{ asset("assets/jquery-file-uploads/js/jquery.ui.widget.js") }}"></script>
    <script src="{{ asset("assets/jquery-file-uploads/js/canvas-to-blob.min.js") }}"></script>
    <script src="{{ asset("assets/jquery-file-uploads/js/jquery.iframe-transport.js") }}"></script>
    <script src="{{ asset("assets/jquery-file-uploads/js/jquery.fileupload.js") }}"></script>
@endsection

@section("js_custom")
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            $(document).on('blur','.albumname', function(event) {
                var input = $("input[name='albumname']");
                var albumname = this.value;
                if (this.value.length > 0) {
                    $.ajax({
                        url: '{{ url("master/gallery/check-albumname")}}',
                        type: "POST",
                        data: {"albumname": albumname},
                        success: function (response) {
                            if (response.status == true) {
                                $(input).parent().removeClass("has-error");
                                $(input).parent().find("span").remove();
                                $('#fileupload').prop('disabled', false);
                            } else {
                                $(input).parent().addClass("has-error");
                                $("<span class='help-block'>Nama Album sudah ada</span>").insertAfter(input);
                                $('#fileupload').prop('disabled', true)
                            }
                        },
                        error: function(xhr) {
                            alert('Terjadi kesalahan, silahkan coba lagi');
                        }
                    });
                } else {
                    $('#fileupload').prop('disabled', true);
                }
            });

            $('.btn-danger').hide();
        });

        $(function () {
            var files = 0;
            $('#fileupload').on('change', function(event) {
                $.each(event.target.files, function(index, file) {
                    files++;
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $('#files_list').show(400);
                        $('#files_list').append("<div id='item-upload-"+files+"'><div class='thumbnail'><img src="+event.target.result+" class='preview'></div></div>");
                    };
                    reader.readAsDataURL(file);
                  });
            }).fileupload({
                dataType: 'json',
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                add: function (e, data) {
                    $('#loading').text('Uploading...');
                    data.submit();
                },
                done: function (e, data) {
                    $.each(data.result.files, function (index, file) {
                        $('#files_list').show(400);
                        $('<p class="size"/>').html(file.name).appendTo($('#item-upload-'+files));
                        $("<a class='btn btn-xs btn-danger'onclick='deleteDetailPhoto(this)' data-id='"+ file.fileID +"' data-path='"+ file.path +"' style='margin-bottom: 20px;'>Hapus</a>").appendTo($('#item-upload-'+files));
                        if ($('#file_ids').val() != '') {
                            $('#file_ids').val($('#file_ids').val() + ',');
                        }
                        $('#file_ids').val($('#file_ids').val() + file.fileID);
                    });

                    $('#path').val(data.result.path);
                    $('.btn-danger').show(400);
                    $('#loading').text('');
                }
            });
        });

        function deleteDetailPhoto(event) {
            var id = $(event).data("id");
            var path = $(event).data("path");
            var full_path = $('#path').val();
            if (confirm('Hapus Foto Ini?')) {
                $.ajax({
                    url: '{{ url("master/gallery/delete-photo")}}/' + id,
                    type: 'DELETE',
                    data: {"id": id, "path": path, "full_path": full_path},
                    success: function(data) {
                        $(event).parent().remove();
                    },
                    error: function(xhr) {
                        alert('Terjadi kesalahan, silahkan coba lagi');
                    }
                });
            }
        }

    </script>
@endsection