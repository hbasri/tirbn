@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/plugins/datepicker/datepicker3.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/select2/css/select2.css") }}">
@endsection
@section("css_custom")
    <style type="text/css">
        .button-action {
            float: left;
            margin-left: 15px;
        }

        .title-action {
            margin-left: 10px;
        }

        .search {
            /*margin-left: 400px;*/
            margin-top: 30px;
            float: right;
        }

        .search .form-control {
            width: 250px;
        }

        .search .btn-default {
            width: 80px;
        }

    </style>
@endsection
@section("content")

    <section class="content-header">
        <h1>
            Galeri
            <small>Data Galeri</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Galeri</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                        <!-- Split button -->
                        <div class="button-action">
                            <span class="title-action">Tambah Galeri</span>

                            <div class="margin">
                                <a href="{{ url("master/gallery/create") }}">
                                    <button type="button" class="btn btn-primary"><i class="fa fa-fw fa-edit"></i>
                                        Galeri
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Galeri</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="20%">Nama Album</th>
                                <th width="25%">Deskripsi</th>
                                <th width="10%">Status</th>
                                <th width="10%"></th>
                            </tr>
                            <tr>
                                <td><input type="text" name="searchAlbumName" class="form-control"></td>
                                <td><input type="text" name="searchDesc" class="form-control"></td>
                                <td>
                                    <select name="searchStatus" class="form-control select2">
                                        <option value="all">Semua</option>
                                        <option value="show">Ditampilkan</option>
                                        <option value="not show">Tidak Ditampilkan</option>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="gallery-body">
                            @include("cms.gallery.gallery")
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix" id="gallery-paginate">
                        @include("cms.gallery.ul")
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>

@endsection
@section("js_plugins")
    <script src="{{ asset("assets/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("assets/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <!-- page script -->
    <script type="text/javascript" charset="utf-8" async defer>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getGallery(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                getGallery($(this).attr('href').split('page=')[1]);
            });
        });

        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "-- Pilih --"
            });

            $('.datepicker').datepicker({
                format: "yyyy/mm/dd"
            });

            $("input[name='searchAlbumName']").on("blur",function(){
                getGallery(1);
            });

            $("input[name='searchDesc']").on("blur",function(){
                getGallery(1);
            });

            $("select[name='searchStatus']").on("change",function(){
                getGallery(1);
            });
        });

        //fungsi
        function getGallery(page) {
            var albumName = $("input[name='searchAlbumName']").val();
            var desc = $("input[name='searchDesc']").val();
            var status = $("select[name='searchStatus']").val();

            $("#gallery-body").html("<tr align='center'><td colspan='4'><img src='{{ asset('assets/loading.gif') }}' height='50px' width='50px'></td></tr>");
            $.ajax({
                url: '?page=' + page + "&albumName=" + albumName + "&desc=" + desc + "&status=" + status,
                dataType: 'json',
            }).done(function (data) {
                $('#gallery-body').html(data.galleries);
                $("#gallery-paginate").html(data.ul_galleries);
                location.hash = page;
            }).fail(function () {
                alert('Terjadi kesalahan, silahkan coba lagi');
            });
        }

    </script>
@endsection