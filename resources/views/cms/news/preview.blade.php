@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/plugins/datepicker/datepicker3.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/select2/css/select2.css") }}">
@endsection
@section("css_custom")
    <style type="text/css">
        .button-action {
            float: left;
            margin-left: 15px;
        }

        .title-action {
            margin-left: 10px;
        }

        .search {
            /*margin-left: 400px;*/
            margin-top: 30px;
            float: right;
        }

        .search .form-control {
            width: 250px;
        }

        .search .btn-default {
            width: 80px;
        }

    </style>
@endsection
@section("content")

    <section class="content-header">
        <h1>
            Berita
            <small>Data Berita</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li><a href="{{ url("master/news") }}">News / Berita</a></li>
            <li class="active">Preview</li>
        </ol>
    </section>
    <section class="content">
        <!-- Default box -->
        <div class="row">
                    <div class="col-md-12">
			          <div class="box box-solid">
			            <div class="box-header with-border">
			              <i class="fa fa-text-width"></i>

			              <h3 class="box-title">Deskripsi</h3>
			            </div>
			            <!-- /.box-header -->
			            <div class="box-body">
			              <dl class="dl-horizontal">
			                <dt>Dibuat Tanggal</dt>
			                <dd>{{ $news->created_at }}</dd>
			                <dt>Dibuat Oleh</dt>
			                <dd>{{ $news->createdBy->name }}</dd>
			                <dt>Terakhir Diubah</dt>
			                <dd>{{ !empty($news->edited_at) ? $news->edited_at : "-" }}</dd>
			                <dt>Diubah Oleh</dt>
			                <dd>{{ !empty($news->edited_by) ? $news->editedBy->name : "-" }}</dd>
			                <dt>Tags</dt>
			                <dd>
			                	@foreach($news->newsTag as $item)
			                		<span class="label label-success">{{ $item->tag->tag }}</span>
			                	@endforeach
			                </dd>
			                <dt>Kategori</dt>
			                <dd>{{ $news->category->category }}</dd>
			                <dt>Status</dt>
			                <dd>{{ $news->status }}</dd>
			              </dl>
			            </div>
			            <!-- /.box-body -->
			          </div>
			          <!-- /.box -->
			        </div>
                </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $news->title }}</h3>
            </div>
            <div class="box-body">
                {!! $news->content !!}
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>


@endsection
@section("js_plugins")
    <script src="{{ asset("assets/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("assets/select2/js/select2.js") }}"></script>
@endsection
