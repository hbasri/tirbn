@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/plugins/datepicker/datepicker3.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/select2/css/select2.css") }}">
@endsection
@section("css_custom")
    <style type="text/css">
        .button-action {
            float: left;
            margin-left: 15px;
        }

        .title-action {
            margin-left: 10px;
        }

        .search {
            /*margin-left: 400px;*/
            margin-top: 30px;
            float: right;
        }

        .search .form-control {
            width: 250px;
        }

        .search .btn-default {
            width: 80px;
        }

    </style>
@endsection
@section("content")

    <section class="content-header">
        <h1>
            Berita
            <small>Data Berita</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">News / Berita</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                        <!-- Split button -->
                        <div class="button-action">
                            <span class="title-action">Tambah Berita</span>

                            <div class="margin">
                                <a href="{{ url("master/news/create") }}">
                                    <button type="button" class="btn btn-primary"><i class="fa fa-fw fa-edit"></i>
                                        Berita
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Berita</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="25%">Judul</th>
                                <th width="15%">Kategori</th>
                                <th width="15%">Status</th>
                                <th>Tanggal Publish</th>
                                <th>Tanggal Dibuat</th>
                                <th width="12%">Aksi</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="searchTitle" class="form-control"></td>
                                <td>
                                    <select name="searchCategory" class="form-control select2">
                                        <option value="all">Semua</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->category }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select name="searchStatus" class="form-control select2">
                                        <option value="all">Semua</option>
                                        <option value="draft">Draf</option>
                                        <option value="publish">Publish</option>
                                        <option value="deleted">Dihapus</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control datepicker" name="searchPublishDate">
                                </td>
                                <td><input type="text" class="form-control datepicker" name="searchCreatedDate"></td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="news-body">
                            @include("cms.news.news")
                            </tbody>
                        </table>

                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Detail News</h4>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-bordered">

                                            <tbody id="news"></tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-right"
                                                data-dismiss="modal">
                                            Tutup
                                        </button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix" id="news-paginate">
                        @include("cms.news.ul")
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>

@endsection
@section("js_plugins")
    <script src="{{ asset("assets/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("assets/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <!-- page script -->
    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getNews(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                getNews($(this).attr('href').split('page=')[1]);
            });
        });

        function getNews(page) {
            $("#news-body").html("<tr align='center'><td colspan='6'><img src='{{ asset('assets/loading.gif') }}' height='50px' width='50px'></td></tr>");
            $.ajax({
                url: '?page=' + page + "&title=" + $("input[name='searchTitle']").val() + "&category=" + $("select[name='searchCategory']").val() + "&status=" + $("select[name='searchStatus']").val() + "&publishedDate=" + $("input[name='searchPublishDate']").val() + "&createdDate=" + $("input[name='searchCreatedDate']").val() ,
                dataType: 'json',
            }).done(function (data) {
                $('#news-body').html(data.news);
                $("#news-paginate").html(data.ul_news);
                location.hash = page;
            }).fail(function () {

            });
        }

        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "-- Pilih --"
            });

            $('.datepicker').datepicker({
                format: "yyyy/mm/dd"
            });

            $("input[name='searchTitle']").on("blur",function(){
                getNews(1);
            });

            $("input[name='searchPublishDate']").on("blur",function(){
                getNews(1);
            });

            $("input[name='searchCreatedDate']").on("blur",function(){
                getNews(1);
            });

            $("select[name='searchCategory']").on("change",function(){
                getNews(1);
            });

            $("select[name='searchStatus']").on("change",function(){
                getNews(1);
            });

        });

    </script>
@endsection