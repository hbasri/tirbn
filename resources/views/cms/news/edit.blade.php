@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/plugins/iCheck/all.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/select2/css/select2.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/bootstrap-tagsinput.css") }}">
    {{--<link rel="stylesheet" href="{{ asset("assets/typeahead.css") }}">--}}
    <style>
        .bootstrap-tagsinput {
            width: 100% !important;
        }
    </style>
@endsection
@section("content")
    <section class="content-header">
        <h1>
            Berita
            <small>Data Berita</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Berita</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Ubah Berita</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ url('master/news/'.$news->id) }}" method="post" onkeypress="return event.keyCode != 13;">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('title') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Judul</label>
                                <input type="text" name="title" value="{{ $news->title }}" class="form-control" placeholder="Judul">
                                @if($errors->has("title"))
                                    <span class="help-block">{{ $errors->first("title") }}</span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('category_id') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Kategori</label>
                                <select name="category_id" class="form-control select2" placeholder="Pilih">
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ $news->category_id == $category->id ? 'selected' : '' }}>{{ $category->category }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has("category_id"))
                                    <span class="help-block">{{ $errors->first("category_id") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('content') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Konten</label>
                                <textarea class="form-control" name="content">{{ $news->content }}</textarea>
                                @if($errors->has("content"))
                                    <span class="help-block">{{ $errors->first("content") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback">
                                <label for="exampleInputEmail1">Tags</label>
                                <input type="text" class="form-control tag" data-role="tagsinput" value="{{ implode(',',$tags_id) }}" name="tags">
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('status') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Status</label>
                                <br/>
                                <label>
                                    <input type="radio" name="status" value="draft" class="flat-red" {{ $news->status == "draft" ? "checked" : "" }}> Draft
                                </label>
                                <label>
                                    <input type="radio" name="status" value="publish" class="flat-red" {{ $news->status == "publish" ? "checked" : "" }}> Publish
                                </label>
                                <label>
                                    <input type="radio" name="status" value="deleted" class="flat-red" {{ $news->status == "deleted" ? "checked" : "" }}> Dihapus
                                </label>

                                @if($errors->has("status"))
                                    <span class="help-block">{{ $errors->first("status") }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ url()->previous() }}">
                                <button type="button" class="btn btn-default">Batal</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("js_plugins")
    <script src="{{ asset("assets/plugins/iCheck/icheck.min.js") }}"></script>
    <script src="{{ asset("assets/select2/js/select2.js") }}"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script src="{{ asset("assets/bootstrap-tagsinput.js") }}"></script>
    {{--<script src="{{ asset("assets/typeahead.min.js") }}"></script>--}}

@endsection

@section("js_custom")
    <script type="text/javascript">
        $(document).ready(function () {


            $(".select2").select2({
                placeholder: "-- Pilih --"
            });

            var options = {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
            };

            $('textarea').ckeditor(options);

            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            $('.tags').tagsinput({
                typeahead: {
                    source: ['Amsterdam', 'Washington', 'Sydney', 'Beijing', 'Cairo']
                },
                freeInput: true
            });

        });

    </script>
@endsection