@if(count($news) > 0)
    @foreach($news as $new)
        <tr>
            <td>{{ (!empty($new->title) ? $new->title : "-") }}</td>
            <td>{{ !empty($new->category) ? $new->category->category : "-" }}</td>
            <td>
                @if($new->status == "draft")
                    <span class="label label-warning">Draf</span>
                @elseif($new->status == "publish")
                    <span class="label label-success">Publish</span>
                @elseif($new->status == "deleted")
                    <span class="label label-default">Dihapus</span>
                @endif
            </td>
            <td>{{ !empty($new->publish_at) ? (string) $new->publish_at : "-" }}</td>
            <td>{{ !empty($new->created_at) ? (string) $new->created_at : "-" }}</td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary">Aksi</button>
                    <button type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url("master/news/show/".$new->id) }}">Preview</a></li>
                        <li><a href="{{ url("master/news/edit/".$new->id) }}">Ubah</a>
                        </li>
                        <li><a href="{{ url("master/news/".$new->id) }}"
                               data-method="delete" data-confirm="Hapus Data Ini?"
                               data-token="{{ csrf_token() }}">Hapus</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="6">Data Masih Kosong</td>
    </tr>
@endif