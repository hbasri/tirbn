@extends("layouts.cms")
@section("css_plugins")

@endsection
@section("css_custom")
    <style type="text/css">
        .button-action {
            float: left;
            margin-left: 15px;
        }

        .title-action {
            margin-left: 10px;
        }

        .search {
            /*margin-left: 400px;*/
            margin-top: 30px;
            float: right;
        }

        .search .form-control {
            width: 250px;
        }

        .search .btn-default {
            width: 80px;
        }

    </style>
@endsection
@section("content")

    <section class="content-header">
        <h1>
            Users
            <small>Data Users</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Users</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                        <!-- Split button -->
                        <div class="button-action">
                            <span class="title-action">Tambah User</span>

                            <div class="margin">
                                <a href="{{ url("master/user/create") }}">
                                    <button type="button" class="btn btn-primary"><i class="fa fa-fw fa-users"></i>
                                        User
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Users</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="25%">Nama</th>
                                <th width="25%">Username</th>
                                <th>Level</th>
                                <th width="12%">Aksi</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="searchName" class="form-control"></td>
                                <td><input type="text" name="searchUsername" class="form-control"> </td>
                                <td>
                                    <select name="searchLevel" class="form-control">
                                        <option disabled selected>Pilih</option>
                                        <option value="all">Semua</option>
                                        <option value="administrator">Administrator</option>
                                        <option value="staff">Staff</option>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="users-body">
                                @include("cms.user.user")
                            </tbody>
                        </table>

                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Detail User</h4>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-bordered">

                                            <tbody id="user"></tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-right"
                                                data-dismiss="modal">
                                            Tutup
                                        </button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix" id="users-paginate">
                        @include("cms.user.ul")
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>

@endsection
@section("js_plugins")

@endsection

@section("js_custom")
    <!-- page script -->
    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getUsers(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                getUsers($(this).attr('href').split('page=')[1]);
            });
        });

        function getUsers(page) {
            $("#users-body").html("<tr align='center'><td colspan='4'><img src='{{ asset('assets/loading.gif') }}' height='50px' width='50px'></td></tr>");
            $.ajax({
                url: '?page=' + page + "&username=" + $("input[name='searchUsername']").val() + "&name=" + $("input[name='searchName']").val() + "&level=" + $("select[name='searchLevel']").val(),
                dataType: 'json',
            }).done(function (data) {
                $('#users-body').html(data.users);
                $("#users-paginate").html(data.ul_users);
                location.hash = page;
            }).fail(function () {

            });
        }

        function lihatDetail(e){
            var id = $(e).attr("data-id");
            $.get('{{ url("master/user/show/") }}/' + id, function (data) {
                    var name, username, level,created_at;
                    var htmlOutput = "<tr>";
                    htmlOutput += "<th width='30%'>Nama</th>";
                    if (data.name === null) {
                        name = "-"
                    } else {
                        name = data.name;
                    }
                    htmlOutput += "<td>" + name + "</td><tr>";

                    htmlOutput += "<tr><th>Username</th>";
                    if (data.username === null) {
                        username = "-"
                    } else {
                        username = data.username;
                    }
                    htmlOutput += "<td>" + username + "</td></tr>";

                    htmlOutput += "<tr><th>Level</th>";
                    if (data.level === null) {
                        level = "-"
                    } else {
                        level = data.level;
                    }
                    htmlOutput += "<td>" + level + "</td></tr>";

                    htmlOutput += "<tr><th>Dibuat Tanggal</th>";
                    if (data.created_at === null) {
                        created_at = "-"
                    } else {
                        created_at = data.created_at;
                    }
                    htmlOutput += "<td>" + created_at + "</td></tr>";


                    $("#user").html(htmlOutput);
                    $("#myModal").modal("show");

            });
        }

        $(document).ready(function () {
            $("input[name='searchName']").on("blur",function(){
               getUsers(1);
            });

            $("input[name='searchUsername']").on("blur",function(){
                getUsers(1);
            });

            $("select[name='searchLevel']").on("change",function(){
                getUsers(1);
            });

        });

    </script>
@endsection