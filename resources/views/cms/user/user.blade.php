@if(count($users) > 0)
    @foreach($users as $user)
        <tr>
            <td>{{ (!empty($user->name) ? $user->name : "-") }}</td>
            <td>{{ !empty($user->username) ? $user->username : "-" }}</td>
            <td>{{ !empty($user->level) ? ucwords($user->level) : "-" }}</td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary">Aksi</button>
                    <button type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#" class="lihat-detail"
                               data-id="{{ $user->id }}" onclick="lihatDetail(this)">Lihat Detail</a></li>
                        <li><a href="{{ url("master/user/edit/".$user->id) }}">Ubah</a>
                        </li>
                        <li><a href="{{ url("master/user/".$user->id) }}"
                               data-method="delete" data-confirm="Hapus Data Ini?"
                               data-token="{{ csrf_token() }}">Hapus</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
@endif