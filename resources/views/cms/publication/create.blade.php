@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/plugins/iCheck/all.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/select2/css/select2.css") }}">
@endsection
@section("content")
    <section class="content-header">
        <h1>
            Publikasi
            <small>Data Publikasi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Publikasi</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Tambah Publikasi</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ url('master/publication') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Nama Publikasi</label>
                                <input type="text" name="name" value="{{ old("name") }}" class="form-control" placeholder="Nama">
                                @if($errors->has("name"))
                                    <span class="help-block">{{ $errors->first("name") }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Deskripsi</label>
                                <textarea class="form-control" name="description">{{ old("description") }}</textarea>

                            </div>
                            <div class="form-group has-feedback {{ $errors->has('file') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">File</label>
                                <input type="file" name="file" class="form-control">
                                @if($errors->has("file"))
                                    <span class="help-block">{{ $errors->first("file") }}</span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('status') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Status Tampil</label>
                                <br/>
                                <label>
                                    <input type="radio" name="status" value="show" class="flat-red" checked> Ditampilkan
                                </label>
                                <label>
                                    <input type="radio" name="status" value="not show" class="flat-red" {{ old("status") == "not show" ? "checked" : "" }}> Tidak Ditampilkan
                                </label>

                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ url()->previous() }}">
                                <button type="button" class="btn btn-default">Batal</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("js_plugins")
    <script src="{{ asset("assets/plugins/iCheck/icheck.min.js") }}"></script>
    <script src="{{ asset("assets/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

        });

    </script>
@endsection