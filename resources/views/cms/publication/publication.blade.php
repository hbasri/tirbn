@if(count($publications) > 0)
    @foreach($publications as $publication)
        <tr>
            <td>{{ (!empty($publication->name) ? $publication->name : "-") }}</td>
            <td>{{ !empty($publication->desctiption) ? $publication->description : "-" }}</td>
            <td>
                @if(!empty($publication->file))
                    <a href="{{ asset('assets/publications/'.$publication->linkfile) }}" target="_blank"><img src="{{ asset("assets/iconfiles/".$publication->extention.".png") }}" width="25px" height="25px"> {{ $publication->file }}</a>
                @else
                    -
                @endif
            </td>
            <td>
                @if($publication->status == "show")
                    <span class="label label-success">Ditampilkan</span>
                @elseif($publication->status == "publish")
                    <span class="label label-danger">Tidak Ditampilkan</span>
                @endif
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary">Aksi</button>
                    <button type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url("master/publication/edit/".$publication->id) }}">Ubah</a>
                        </li>
                        <li><a href="{{ url("master/publication/".$publication->id) }}"
                               data-method="delete" data-confirm="Hapus Data Ini?"
                               data-token="{{ csrf_token() }}">Hapus</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="6">Data Masih Kosong</td>
    </tr>
@endif