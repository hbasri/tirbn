@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/plugins/datepicker/datepicker3.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/select2/css/select2.css") }}">
@endsection
@section("css_custom")
    <style type="text/css">
        .button-action {
            float: left;
            margin-left: 15px;
        }

        .title-action {
            margin-left: 10px;
        }

        .search {
            /*margin-left: 400px;*/
            margin-top: 30px;
            float: right;
        }

        .search .form-control {
            width: 250px;
        }

        .search .btn-default {
            width: 80px;
        }

    </style>
@endsection
@section("content")
    
    <section class="content-header">
        <h1>
            Publikasi
            <small>Data Publikasi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Publikasi</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                        <!-- Split button -->
                        <div class="button-action">
                            <span class="title-action">Tambah Publikasi</span>

                            <div class="margin">
                                <a href="{{ url("master/publication/create") }}">
                                    <button type="button" class="btn btn-primary"><i class="fa fa-fw fa-edit"></i>
                                        Publikasi
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Publikasi</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="20%">Nama</th>
                                <th width="25%">Deskripsi</th>
                                <th width="15%">File</th>
                                <th width="10%">Status</th>
                                <th width="10%"></th>
                            </tr>
                            <tr>
                                <td><input type="text" name="searchName" class="form-control"></td>
                                <td><input type="text" name="searchDescription" class="form-control"></td>
                                <td><input type="text" name="searchFile" class="form-control"></td>
                                <td>
                                    <select name="searchStatus" class="form-control select2">
                                        <option value="all">Semua</option>
                                        <option value="show">Ditampilkan</option>
                                        <option value="not show">Tidak Ditampilkan</option>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="publication-body">
                            @include("cms.publication.publication")
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix" id="publication-paginate">
                        @include("cms.publication.ul")
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>

@endsection
@section("js_plugins")
    <script src="{{ asset("assets/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("assets/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <!-- page script -->
    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getPublication(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                getPublication($(this).attr('href').split('page=')[1]);
            });
        });

        function getPublication(page) {
            $("#publication-body").html("<tr align='center'><td colspan='5'><img src='{{ asset('assets/loading.gif') }}' height='50px' width='50px'></td></tr>");
            $.ajax({
                url: '?page=' + page + "&name=" + $("input[name='searchName']").val() + "&description=" + $("input[name='searchDescription']").val() + "&file=" + $("input[name='searchFile']").val() + "&status=" + $("select[name='searchStatus']").val(),
                dataType: 'json',
            }).done(function (data) {
                $('#publication-body').html(data.publications);
                $("#publication-paginate").html(data.ul_publications);
                location.hash = page;
            }).fail(function () {

            });
        }

        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "-- Pilih --"
            });

            $('.datepicker').datepicker({
                format: "yyyy/mm/dd"
            });

            $("input[name='searchName']").on("blur",function(){
                getPublication(1);
            });

            $("input[name='searchDescription']").on("blur",function(){
                getPublication(1);
            });

            $("input[name='searchFile']").on("blur",function(){
                getPublication(1);
            });

            $("select[name='searchStatus']").on("change",function(){
                getPublication(1);
            });
        });

    </script>
@endsection