<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PublicationRequest;
use App\Publication;
use File;

class PublicationController extends Controller
{
    //
    public function index(Request $request){
        $data['title'] = "Publications";
        $data['publications'] = Publication::orderBy("created_at","desc")->paginate(10);

        if ($request->ajax()) {
            $name = "";
            $description = "";
            $file = "";
            $status ="";

            if (isset($_GET['name'])) {
                if (!empty($_GET['name'])) {
                    $name = trim($_GET['name']);
                }
            }

            if (isset($_GET['description'])) {
                if (!empty($_GET['description'])) {
                    $description = trim($_GET['description']);
                }
            }

            if (isset($_GET['file'])) {
                if (!empty($_GET['file'])) {
                    $file = trim($_GET['file']);
                }
            }

            if (isset($_GET['status'])) {
                if (!empty($_GET['status'])) {
                    $status = trim($_GET['status']);
                }
            }

            $publications = Publication::search($name,$description,$file,$status)->paginate(10);
            $output['publications'] = view("cms.publication.publication", ['publications' => $publications])->render();
            $output['ul_publications'] = view("cms.publication.ul", ['publications' => $publications])->render();

            return response()->json($output, 200);
        }

        return view("cms.publication.index",$data);
    }

    public function create(){
        $data['title'] = "Create Publication";
        return view("cms.publication.create",$data);
    }

    public function save(PublicationRequest $request){
        $publication = new Publication();
        $publication->name = $request->input("name");
        $publication->description = $request->input("description");
        $publication->status = $request->input("status");

        if($request->hasFile("file")){
            $file = $request->file("file");
            $filename = $file->getClientOriginalName();
            $linkfile = md5(uniqid().$filename).".".$file->getClientOriginalExtension();
            $destinationPath = "assets/publications/";
            $file->move($destinationPath,$linkfile);
            $publication->file = $filename;
            $publication->linkfile = $linkfile;
            $publication->extention = $file->getClientOriginalExtension();
        }

        $publication->save();

        return redirect("master/publications")->with("success","Berhasil tambah publikasi");

    }

    public function delete($id){
        Publication::where("id",$id)->delete();
        return redirect("master/publications")->with("success","Berhasil hapus publikasi");
    }

    public function edit($id){
        $publication = Publication::find($id);
        $data['title'] = "Edit Publication";
        $data['publication'] = $publication;
        return view("cms.publication.edit",$data);
    }

    public function update($id,PublicationRequest $request){
        $publication = Publication::find($id);
        $publication->name = $request->input("name");
        $publication->description = $request->input("description");
        $publication->status = $request->input("status");

        if($request->hasFile("file")){
            $file = $request->file("file");
            $filename = $file->getClientOriginalName();
            $linkfile = md5(uniqid().$filename).".".$file->getClientOriginalExtension();
            $destinationPath = "assets/publications/";
            $file->move($destinationPath,$linkfile);
            $publication->file = $filename;
            $publication->linkfile = $linkfile;
            $publication->extention = $file->getClientOriginalExtension();
        }

        $publication->save();
        return redirect("master/publications")->with("success","Berhasil ubah publikasi");
    }

    public function deleteFile($id){
        $publication = Publication::find($id);
        $publication->file = "";
        $publication->linkfile = "";
        $publication->extention = "";

        $file = "assets/publication/".$publication->linkfile;
        File::delete($file);

        $publication->save();

        return redirect()->back();
    }
}
