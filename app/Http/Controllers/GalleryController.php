<?php

namespace App\Http\Controllers;

use DB;
use File;
use Storage;
use Validator;
use Carbon\Carbon;
use App\Album;
use App\DetailAlbum;
use Illuminate\Http\Request;
use App\Http\Requests\GalleryRequest;

class GalleryController extends Controller
{
    //
    public function test(){
        return view("test.hasan",$data);
    }

    public function index(Request $request){
        $data['title'] = "Galleries";
        $data['galleries'] =  Album::orderBy('created_at','desc')->paginate(10);

        if ($request->ajax()) {
            $albumName = '';
            $desc = '';
            $status = '';

            if (isset($_GET['albumName'])) {
                if (!empty($_GET['albumName'])) {
                    $albumName = trim($_GET['albumName']);
                }
            }

            if (isset($_GET['desc'])) {
                if (!empty($_GET['desc'])) {
                    $desc = trim($_GET['desc']);
                }
            }

            if (isset($_GET['status'])) {
                if (!empty($_GET['status'])) {
                    $status = trim($_GET['status']);
                }
            }

            $galleries = Album::search($albumName, $desc, $status)->paginate(10);
            $output['galleries'] = view('cms.gallery.gallery', ['galleries' => $galleries])->render();
            $output['ul_galleries'] = view('cms.gallery.ul', ['galleries' => $galleries])->render();

            return response()->json($output, 200);
        }

        return view('cms.gallery.index', $data);
    }

    public function create(){
        $data['title'] = 'Create Gallery';

        return view('cms.gallery.create', $data);
    }

    public function store(GalleryRequest $request){
        try {
            DB::beginTransaction();

            Album::where('albumname', $request->input('albumname'))
                ->update([
                    'albumname' => $request->input('albumname'),
                    'desc' => $request->input('desc'),
                    'status' => $request->input('status'),
                    'status_upload' => $request->input('status_upload')
                ]);

            DB::commit();

            return redirect("master/gallery")->with("success", "Berhasil tambah album");

        } catch (Exception $e) {
            DB::rollback();

            return redirect()->back()->with("error", "Ada kesalahan, silahkan coba lagi nanti.");
        }
    }

    public function edit($id){
        $album = Album::select(
            'albums.id',
            'albums.albumname',
            'albums.desc',
            'albums.status',
            'detail_albums.album_id'
        )
        ->where('albums.id', $id)
        ->leftJoin('detail_albums', 'detail_albums.album_id', '=', 'albums.id')
        ->first();

        $detailAlbum = DetailAlbum::where('album_id', $id)->get();

        $data['title'] = 'Edit Gallery';
        $data['album'] = $album;
        $data['detailAlbums'] = $detailAlbum;

        return view('cms.gallery.edit', $data);
    }

    public function update($id, GalleryRequest $request){
        try {
            DB::beginTransaction();

            $album = Album::find($id);
            $album->albumname = $request->input("albumname");
            $album->desc = $request->input("desc");
            $album->status = $request->input("status");
            $album->save();

            File::move($request->input('path'), "assets/albums/".$request->input('albumname'));

            DB::commit();

            return redirect("master/gallery")->with("success", "Berhasil ubah album");

        } catch (Exception $e) {
            DB::rollback();

            return redirect()->back()->with("error", "Ada kesalahan, silahkan coba lagi nanti.");
        }
    }

    public function delete($id){
        $album = Album::find($id);
        $detailAlbums = DetailAlbum::where('album_id', $album->id)->get();
        foreach ($detailAlbums as $detailAlbum) {
            $file = "assets/albums/".$album->albumname;
            File::deleteDirectory($file);
            $detailAlbum->file = "";
            $detailAlbum->linkfile = "";
            $detailAlbum->deleted_at = Carbon::now();
            $detailAlbum->save();
        }

        $album->delete();

        return redirect("master/gallery")->with("success", "Berhasil hapus album");
    }

    public function uploads(Request $request){
        if($request->photos){
            try {
                DB::beginTransaction();

                $album = Album::where("status_upload", "draft")->first();
                if (count($album) == 0) {
                    $album = new Album();
                    $album->albumname = $request->input('albumname');
                    $album->save();
                }

                $photos = [];
                    foreach ($request->photos as $photo) {
                        $filename = $photo->store('photos');
                        $originalName = str_replace('photos/', '',$photo->getClientOriginalName());
                        $linkfile = md5(uniqid().$originalName).".".$photo->getClientOriginalExtension();
                        $destinationPath = "assets/albums/".$request->input('albumname');
                        $photo->move($destinationPath,$linkfile);

                        $detailAlbum = new DetailAlbum();
                        $detailAlbum->file = $originalName;
                        $detailAlbum->linkfile = $linkfile;
                        $detailAlbum->album_id = $album->id;
                        $detailAlbum->save();

                        $tempPhoto = new \stdClass();
                        $tempPhoto->name = $originalName;
                        $tempPhoto->fileID = $detailAlbum->id;
                        $tempPhoto->path = "assets/albums/".$request->input('albumname')."/".$detailAlbum->linkfile;
                        $photos[] = $tempPhoto;
                    }

                    DB::commit();

                    return response()->json(['files' => $photos, 'path' => $destinationPath]);
            } catch (Exception $e) {
                DB::rollback();
                $errors = [
                    'status' => false,
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine()
                ];

                return response()->json($errors);
            }
        }
    }

    public function uploadById($id, Request $request){
        if($request->photos){
            try {
                DB::beginTransaction();

                $album = Album::where(["id" => $id, "status_upload" => "done"])->first();
                if (!$album) {
                    $album = new Album();
                    $album->albumname = $request->input('albumname');
                    $album->save();
                }

                $photos = [];
                    foreach ($request->photos as $photo) {
                        $filename = $photo->store('photos');
                        $originalName = str_replace('photos/', '',$photo->getClientOriginalName());
                        $linkfile = md5(uniqid().$originalName).".".$photo->getClientOriginalExtension();
                        $destinationPath = "assets/albums/".$request->input('albumname');
                        $photo->move($destinationPath,$linkfile);

                        $detailAlbum = new DetailAlbum();
                        $detailAlbum->file = $originalName;
                        $detailAlbum->linkfile = $linkfile;
                        $detailAlbum->album_id = $album->id;
                        $detailAlbum->save();

                        $tempPhoto = new \stdClass();
                        $tempPhoto->name = $originalName;
                        $tempPhoto->fileID = $detailAlbum->id;
                        $tempPhoto->path = "assets/albums/".$request->input('albumname')."/".$detailAlbum->linkfile;
                        $photos[] = $tempPhoto;
                    }

                    DB::commit();

                    return response()->json(['files' => $photos, 'path' => $destinationPath]);
            } catch (Exception $e) {
                DB::rollback();
                $errors = [
                    'status' => false,
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine()
                ];

                return response()->json($errors);
            }
        }
    }

    public function deletePhoto($id, Request $request){
        $tempDetailAlbums = [];
        $detailAlbums = DetailAlbum::where('id', $id)->get();
        foreach ($detailAlbums as $detailAlbum) {
            $detailAlbum->file = "";
            $detailAlbum->linkfile = "";
            $detailAlbum->deleted_at = Carbon::now();
            $detailAlbum->save();

            array_push($tempDetailAlbums, $detailAlbum->album_id);
        }

        File::delete($request->path);

        $existDetailAlbums = DetailAlbum::whereIn('album_id', $tempDetailAlbums)->count();
        if ($existDetailAlbums == 0) {
            File::deleteDirectory($request->full_path);
        }
    }

    public function validateAlbumnameExist(Request $request){
        $exists = Album::where('albumname', $request->albumname)->first();

        if ($exists) {
            return response()->json(['status' => false]);
        }

        return response()->json(['status' => true]);
    }

    public function validateAlbumnameExistById($id, Request $request){
        $exists = Album::where('albumname', $request->albumname)
            ->where(function($query) use ($id){
                if($id!=''){
                    $query->where('id', '!=', $id);
                }
            })
            ->first();

        if ($exists) {
            return response()->json(['status' => false]);
        }

        return response()->json(['status' => true]);
    }
}
