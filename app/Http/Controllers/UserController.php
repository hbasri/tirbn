<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    //
    public function index(Request $request){
        $data['title'] = "User";
        $data['users'] = User::orderBy("id","desc")->paginate(10);

        if ($request->ajax()) {
            $username = "";
            $name = "";
            $level = "";

            if (isset($_GET['username'])) {
                if (!empty($_GET['username'])) {
                    $username = trim($_GET['username']);
                }
            }

            if (isset($_GET['name'])) {
                if (!empty($_GET['name'])) {
                    $name = trim($_GET['name']);
                }
            }

            if (isset($_GET['level'])) {
                if (!empty($_GET['level']) && $_GET['level'] != "null") {
                    $level = trim($_GET['level']);
                }
            }

            $users = User::search($username,$name,$level)->paginate(10);
            $output['users'] = view("cms.user.user", ['users' => $users])->render();
            $output['ul_users'] = view("cms.user.ul", ['users' => $users])->render();
            $output['l'] = $level;

            return response()->json($output, 200);
        }

        return view("cms.user.index",$data);
    }

    public function show($id){
        $user = User::find($id);
        $output=[];
        $output['name'] = $user->name;
        $output['username'] = $user->username;
        $output['created_at'] = (string) $user->created_at;
        $output['level'] = $user->level;

        return response()->json($output,200);
    }

    public function save(Request $request){

    }

    public function create(){
        $data['title'] = "Create User";

        return view("cms.user.create",$data);
    }

    public function store(UserRequest $request){
        $user = new User();
        $user->name = $request->input("name");
        $user->username = $request->input("username");
        $user->level = $request->input("level");
        $user->password = bcrypt($request->input("password"));
        $user->save();

        return redirect("master/users")->with("success","Berhasil tambah user.");
    }

    public function edit($id){
        $data['title'] = "Edit User";
        $data['user'] = User::find($id);

        return view("cms.user.edit",$data);

    }

    public function update(UserRequest $request,$id){
        $user = User::find($id);
        $user->name = $request->input("name");
        $user->level = $request->input("level");
        $user->save();

        return redirect("master/users")->with("success","Berhasil ubah user.");
    }

    public function delete($id){
        $user = User::find($id)->delete();
        return redirect("master/users")->with("success","Berhasil hapus user.");
    }
}
