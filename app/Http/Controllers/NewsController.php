<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewsRequest;
use App\News;
use App\Category;
use App\NewsTag;
use App\Tag;
use Auth;
use DB;

class NewsController extends Controller
{
    //

    public function index(Request $request){
        $data['title'] = "News";
        $data['categories'] = Category::all();
        $data['news'] = News::orderBy("created_at","desc")->paginate(10);

        if ($request->ajax()) {
            $title = "";
            $category = "";
            $status = "";
            $publishedDate = "";
            $createdDate = "";

            if (isset($_GET['title'])) {
                if (!empty($_GET['title'])) {
                    $title = trim($_GET['title']);
                }
            }

            if (isset($_GET['category'])) {
                if (!empty($_GET['category'])) {
                    $category = trim($_GET['category']);
                }
            }

            if (isset($_GET['status'])) {
                if (!empty($_GET['status']) && $_GET['status'] != "null") {
                    $status = trim($_GET['status']);
                }
            }

            if (isset($_GET['publishedDate'])) {
                if (!empty($_GET['publishedDate'])) {
                    $publishedDate = trim($_GET['publishedDate']);
                }
            }

            if (isset($_GET['createdDate'])) {
                if (!empty($_GET['createdDate'])) {
                    $createdDate = trim($_GET['createdDate']);
                }
            }

            $news = News::search($title,$category,$status,$publishedDate,$createdDate)->paginate(10);
            $output['news'] = view("cms.news.news", ['news' => $news])->render();
            $output['ul_news'] = view("cms.news.ul", ['news' => $news])->render();

            return response()->json($output, 200);
        }

        return view("cms.news.index",$data);
    }

    public function create(){
        $data['title'] = "Create News";
        $data['categories'] = Category::all();

        return view("cms.news.create",$data);
    }

    public function store(NewsRequest $request){
        try {
            DB::beginTransaction();

            $news = new News();
            $news->title = $request->input("title");
            $news->category_id = $request->input("category_id");
            $news->content = $request->input("content");
            $news->status = $request->input("status");
            $news->created_by = Auth::user()->id;
            $news->created_at = date("Y-m-d H:i:s");
            if($request->input("status") == "publish") $news->publish_at = date("Y-m-d H:i:s");
            $news->save();

            $tags = $request->input("tags");
            if(!empty($tags)){
                $tags = explode(",",$tags);
                foreach($tags as $tag){
                    $existTag = Tag::where("tag",$tag)->first();
                    if(count($existTag) == 0){
                        $existTag = new Tag();
                        $existTag->tag = $tag;
                        $existTag->save();
                    }

                    $newstags = new NewsTag();
                    $newstags->news_id = $news->id;
                    $newstags->tag_id = $existTag->id;
                    $newstags->save();
                }
            }

            DB::commit();

            return redirect("master/news")->with("success","Berhasil tambah berita");

        }catch(Exception $e){
            DB::rollback();
            return redirect()->back()->with("error","Ada kesalahan, silahkan coba lagi nanti.");
        }
    }

    public function edit($id){
        $news = News::find($id);
        $tagId = $news->newsTag->map(function($item,$key){
            return $item->tag->tag;
        })->toArray();
        $data['news'] = $news;
        $data['tags_id'] = $tagId;
        $data['title'] = "News - $news->title";
        $data['categories'] = Category::all();
        
        return view("cms.news.edit",$data);
    }

    public function update($id,NewsRequest $request){
        try {
            DB::beginTransaction();

            $news = News::find($id);
            $news->title = $request->input("title");
            $news->category_id = $request->input("category_id");
            $news->content = $request->input("content");
            $news->status = $request->input("status");
            $news->updated_at = date("Y-m-d H:i:s");
            $news->edited_by = Auth::user()->id;
            if($request->input("status") == "publish") $news->publish_at = date("Y-m-d H:i:s");
            $news->save();

            $tags = $request->input("tags");
            $newsTagsId = [];

            if(!empty($tags)){
                $tags = explode(",",$tags);
                foreach($tags as $tag){
                    $existTag = Tag::where("tag",$tag)->first();
                    if(count($existTag) == 0){
                        $existTag = New Tag();
                        $existTag->tag = $tag;
                        $existTag->save();
                    }
                    
                    $existNewsTag = NewsTag::where(["news_id" => $id, "tag_id" => $existTag->id ])->first();
                    if(count($existNewsTag) == 0){
                        $existNewsTag = new NewsTag();
                        $existNewsTag->news_id = $id;
                        $existNewsTag->tag_id = count($existTag) > 0 ? $existTag->id : $newTag->id;
                        $existNewsTag->save();
                    }

                    $newsTagsId[] = $existNewsTag->id;
                }
            }

            NewsTag::where("news_id",$id)->whereNotIn("id",$newsTagsId)->delete();

            DB::commit();

            return redirect("master/news")->with("success","Berhasil ubah berita");

        }catch (Exception $e){
            DB::rollback();
            return redirect()->back()->with("error","Ada kesalahan, silahkan coba lagi nanti.");
        }
    }

    public function show($id){
        $news = News::find($id); 
        $data['news'] = $news;
        $data['title'] = "Preview - News - ".$news->title;

        return view("cms.news.preview",$data);
    }

    public function delete($id){
        News::where("id",$id)->delete();

        return redirect("master/news")->with("success","Berhasil hapus berita");
    }
}
