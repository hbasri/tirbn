<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()){
            case 'POST' :
                return [
                    "username" => 'required|unique:users,username|max:50',
                    "password" => 'required|min:4|confirmed',
                    "password_confirmation" => "required",
                    "level" => "required",
                    "name" => "required"
                ];
                break;

            case 'PATCH' :
                return [
                    "name" => "required",
                    "level" => "required"
                ];
        }
    }
}
