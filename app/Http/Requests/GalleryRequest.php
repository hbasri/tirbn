<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'albumname' => 'required',
            'file_ids' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'albumname.required' => 'Nama Album harus diisi',
            'file_ids.required' => 'Foto harus diisi'
        ];
    }
}
