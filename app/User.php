<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $date = ['deleted_at'];


    public function scopeSearch($query,$username="",$name="",$level=""){
        if(!empty($username)){
            $query->where("username","like","%".$username."%");
        }

        if(!empty($name)){
            $query->where("name","like","%".$name."%");
        }

        if(!empty($level)){
            if($level != "all") $query->where("level",$level);
        }

        $query->orderBy("id","desc");

        return $query;
    }
}
