<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $table = "news";
    protected $dates = ['deleted_at'];

    public function category(){
        return $this->belongsTo(Category::class,"category_id");
    }

    public function newsTag(){
        return $this->hasMany(NewsTag::class,"news_id");
    }

    public function createdBy(){
        return $this->belongsTo(User::class,"created_by");
    }

    public function deletedBy(){
        return $this->belongsTo(User::class,"deleted_by");
    }

    public function editedBy(){
        return $this->belongsTo(User::class,"edited_by");
    }

    public function scopeSearch($query,$title="",$category="",$status="",$publishedDate="",$createdDate=""){
        if(!empty($title))
            $query->where("title","like","%".$title."%");

        if(!empty($category))
            if($category != "all")
                $query->where("category_id",$category);

        if(!empty($status))
            if($status != "all")
                $query->where("status",$status);

        if(!empty($publishedDate))
            $query->where("published_at",date("Y-m-d",strtotime($publishedDate)));

        if(!empty($createdDate))
            $query->where("created_at",date("Y-m-d",strtotime($createdDate)));

        $query->orderBy("created_at","desc");

        return $query;
    }

}
