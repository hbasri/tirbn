<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{
    //
    use SoftDeletes;

    protected $table = "albums";
    protected $dates = ['deleted_at'];

    public function detailAlbums(){
        return $this->hasMany(DetailAlbum::class,"album_id");
    }

    public function scopeSearch($query, $albumName, $desc, $status){
        if (!empty($albumName)) {
            $query->where("albumname","like","%".$albumName."%");
        }

        if (!empty($desc)) {
            $query->where("desc","like","%".$desc."%");
        }

        if (!empty($status)) {
            if ($status != "all") {
                $query->where("status",$status);
            }
        }

        $query->orderBy("created_at","desc");

        return $query;
    }
}
