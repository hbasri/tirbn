<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publication extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public $table = "publications";


    public function scopeSearch($query,$name,$description,$file,$status){
        if(!empty($name))
            $query->where("name","like","%".$name."%");

        if(!empty($description))
            $query->where("description","like","%".$description."%");

        if(!empty($file))
            $query->where("file","like","%".$file."%");

        if(!empty($status))
            if($status != "all")
                $query->where("status",$status);

        $query->orderBy("created_at","desc");

        return $query;
    }
}
