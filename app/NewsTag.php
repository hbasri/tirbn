<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsTag extends Model
{
    protected $table = "news_tags";
    public $timestamps = false;

    public function news(){
        return $this->belongsTo(News::class,"news_id");
    }

    public function tag(){
        return $this->belongsTo(Tag::class,"tag_id");
    }


}
