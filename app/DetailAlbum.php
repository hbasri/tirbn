<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailAlbum extends Model
{
    //
    use SoftDeletes;

    protected $table = "detail_albums";
    protected $dates = ['deleted_at'];

    public function album(){
        return $this->belongsTo(Album::class,"album_id");
    }
}
