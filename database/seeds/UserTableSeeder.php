<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User();
        $user->name = "Hasan";
        $user->password = bcrypt("hasan");
        $user->username = "hasan";
        $user->level = "administrator";
        $user->save();

        $user2 = new User();
        $user2->name = "Basri";
        $user2->password = bcrypt("basri");
        $user2->username = "basri";
        $user2->level = "staff";
        $user2->save();
    }
}
